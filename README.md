# Pull Request Assistant

pull-request-assistant aims help you with **pull request operations from you command line** across different git repository providers. i.e. bitbucket, github, code commit, etc

- :interrobang: For now it only supports **creation of pull** request on **bit bucket**. 
- :interrobang: It remembers your credentials for 15 min and forgets them. It Can be configure on `src/child.ts` for now. 

![](docs/demo.gif)

## Binary Files

You can download the binary builds if you don't want to build from source. Include this binary in your PATH env or alias config to be able to call it globally

- [Windows](https://sourceforge.net/projects/pull-request-assistant/files/windows.7z/download)
- [MacOs](https://sourceforge.net/projects/pull-request-assistant/files/macos.7z/download)
- [Linux](https://sourceforge.net/projects/pull-request-assistant/files/linux.7z/download)

## Getting Started

Build from source and make it global
```
npm install && npm run build && npm link
```
Now go to your git enabled directory and run `pra`

## Road Map

- :white_check_mark: Create binaries for windows, linux, and macOs
- :white_large_square: Migrate configurations to json. Binary build should respect the json config
- :white_large_square: Support pull request creation in code commit
- :white_large_square: Support most used pull request operations in bit bucket
- :white_large_square: Support most used pull request operations in code commit
- :white_large_square: Support pull request creation in github
- :white_large_square: Support most used pull request operations in github




