import pm2 from './lib/pm2-promise'
import killChild from './utils/killChild'
import justLogErrorMessage from './utils/justLogErrorMessage'
import * as shell from 'shelljs'
import * as inquirer from 'inquirer'
import * as path from 'path'
import child from './child'
import config from './config'

const { childName, processNames } = config
const buildName = process.title

let childStartConfig = {
  script: buildName,
  name: childName,
  args: childName,
  interpreter: 'none',
  output: './child.log',
  error: './child.log'
}

if (process.env.NODE_ENV && process.env.NODE_ENV.includes('DEV')) {
  childStartConfig = {
    ...childStartConfig,
    script: path.resolve(__dirname, __filename),
    interpreter: 'node'
  }
}

const lastArgument = process.argv.slice().pop()

if (lastArgument && lastArgument.includes('child')) {
  child()
} else {
  shell.config.silent = true

  pm2.connect(async function (err) {
    if (err) {
      console.error('connect error', err)
      process.exit(2)
    }

    // await killChild()
    const list = await pm2.list()
    const childProperties = list.find(child => child.name === childName)

    // Send Message to Existing Child
    if (childProperties) {
      const pmId = childProperties.pm2_env.pm_id
      const pmStatus = childProperties.pm2_env.status
      if (pmStatus.includes('errored')) {
        console.error('child error')
        await killChild()
        process.exit(2)
      }
      await sendMessageToChild(pmId)
      return
    }

    // Create Child and Send Message
    try {
      const [childStartedProperties] = await pm2.start(childStartConfig)
      const pmId = childStartedProperties.pm2_env.pm_id
      await sendMessageToChild(pmId)
    } catch (e) {
      console.log(e)
    }
  })

  const sendMessageToChild = async (pmId, data = [], topic = 'ping') => {
    const remoteUrl = shell.exec('git remote get-url origin')
    const repoPath = remoteUrl.stdout.match(/(?:https:\/\/)?.+@bitbucket.org[:/]?(.+)\.git/i)[1]
    const currentBranch = shell.exec('git rev-parse --abbrev-ref HEAD').stdout.replace('\n', '')
    pm2.sendDataToProcessId(pmId, {
      type: processNames.messageFromMain,
      data: {
        data,
        repoPath,
        currentBranch
      },
      topic
    }).catch(justLogErrorMessage)
  }

  pm2.launchBus(function (err, bus) {
    bus.on(processNames.askCred, async function (packet) {
      const pmId = packet.process.pm_id
      const { username } = await inquirer.prompt([{
        type: 'input',
        name: 'username',
        message: 'bit bucket username'
      }])
      const { password } = await inquirer.prompt([{
        type: 'password',
        name: 'password',
        message: 'bit bucket password'
      }])
      await sendMessageToChild(pmId, [username, password], processNames.giveCred)
    })

    bus.on(processNames.responseBitBucket, async function (packet) {
      console.log('responseBitBucket', packet.data)
      if ([401].includes(packet.data.response.statusCode)) {
        await killChild()
      }
      pm2.disconnect()
    })
  })
}