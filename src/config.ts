export default {
  childName: 'child',
  processNames: {
    messageFromMain: 'process:msg-from-main',
    askCred: 'process:ask-cred',
    giveCred: 'process:give-cred',
    responseBitBucket: 'process:response-bit-bucket'
  },
  bitBucketPullRequestDefaults: {
    reviewers: [], //[{'username': 'joshua-ignacio'}]
    destinationBranch: 'master'
  }
}