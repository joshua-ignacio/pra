const justLogErrorMessage = (e) => {
  console.log('ERROR: ', e.message)
}

export default justLogErrorMessage