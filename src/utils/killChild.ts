import pm2 from '../lib/pm2-promise'
import config from '../config'
import justLogErrorMessage from './justLogErrorMessage'

const { childName } = config

const killChild = async () => {
  try {
    await pm2.delete(childName).catch(justLogErrorMessage)
  } catch (e) {
    console.log(e)
  }
}

export default killChild