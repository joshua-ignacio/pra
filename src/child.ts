import killChild from './utils/killChild'
import rp from 'request-promise-native'
import opn from 'opn'
import config from './config'

export default () => {
  const { processNames, bitBucketPullRequestDefaults } = config
  let usernameGlobal, passwordGlobal, currentBranchGlobal, repoPathGlobal

  async function requestBitBucketPullRequest() {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      uri: `https://${usernameGlobal}:${passwordGlobal}@bitbucket.org/api/2.0/repositories/${repoPathGlobal}/pullrequests`,
      body: {
        'title': currentBranchGlobal,
        'source': {
          'branch':
            {
              'name': currentBranchGlobal
            },
          'repository': {
            'full_name': repoPathGlobal
          }
        },
        'destination': {
          'branch': {
            'name': bitBucketPullRequestDefaults.destinationBranch
          }
        },
        'reviewers': bitBucketPullRequestDefaults.reviewers,
        'close_source_branch': false
      },
      json: true
    }

    const response = await rp(options)
    const htmlLink = response.links.html.href
    await opn(htmlLink)
    return htmlLink
  }

  process.on('message', async function (packet) {
    const topic = packet.topic
    repoPathGlobal = packet.data.repoPath
    currentBranchGlobal = packet.data.currentBranch

    if (topic === processNames.giveCred) {
      const [username, password] = packet.data.data
      usernameGlobal = username
      passwordGlobal = password
    }

    if (!usernameGlobal || !passwordGlobal) {
      sendMessageToMain(processNames.askCred)
    } else {

      let response = null
      try {
        response = await requestBitBucketPullRequest()
      } catch (e) {
        response = e
      }
      sendMessageToMain(processNames.responseBitBucket,
        {
          response
        })

    }
  })

  const sendMessageToMain = (topic, data?) => {
    process.send({
      type: topic,
      data: {
        topic,
        ...data
      }
    })
  }

// Kill after some time
  setTimeout(killChild, (900 * 1000))
}

